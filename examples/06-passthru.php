<?php

require __DIR__.'/../vendor/autoload.php';

class MyClass
{
    public function getNumber(): int
    {
	return 5;
    }
}

// vytvoří mock objekt
$mock = Mockery::mock(MyClass::class);

// nastaví volání reálné metody getNumber (vrátí hodnotu 5)
$mock->shouldReceive('getNumber')->passthru();

// vypíše int(5)
var_dump($mock->getNumber());

Mockery::close();
