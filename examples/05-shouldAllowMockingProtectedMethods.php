<?php

require __DIR__.'/../vendor/autoload.php';

class MyClass
{
    protected function getNumber(): int
    {
	return 5;
    }
}

// vytvoří mock objekt
$mock = Mockery::mock(MyClass::class)->shouldAllowMockingProtectedMethods();

// nastaví očekávané chování metody getNumber (vrátí hodnotu 3)
$mock->shouldReceive('getNumber')->andReturn(3);

// vypíše int(3)
var_dump($mock->getNumber());

Mockery::close();
