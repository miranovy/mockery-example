<?php

require __DIR__.'/../vendor/autoload.php';

$n=2;

// vytvoří mock objekt
$mock = Mockery::mock();

// nastaví očekávané chování metody plus, která je volání přávě n krát (vrátí hodnotu 3)
$mock->shouldReceive('getNumber')
    ->times($n)
    ->andReturn(3);

// první volání vypíše int(3)
var_dump($mock->getNumber());

// druhé volání vypíše int(3)
var_dump($mock->getNumber());

// třetí volání vypíše int(3)
var_dump($mock->getNumber());

// vyhodí exception Uncaught Mockery\Exception\InvalidCountException: Method getNumber(<Any Arguments>) from Mockery_0 should be called
Mockery::close();
