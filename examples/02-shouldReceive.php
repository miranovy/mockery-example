<?php

require __DIR__.'/../vendor/autoload.php';

// vytvoří mock objekt
$mock = Mockery::mock();

// nastaví očekávané chování metody getNumber - vrátí hodnotu 5
$mock->shouldReceive('getNumber')->andReturn(5);

// vypíše int(5)
var_dump($mock->getNumber());
