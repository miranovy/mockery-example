<?php

require __DIR__.'/../vendor/autoload.php';

// vytvoří mock objekt
$mock = Mockery::mock();

// nastaví očekávané chování metody plus - vrátí hodnotu 3
$mock->shouldReceive('plus')->with(1,2)->andReturn(3);

// vypíše int(3)
var_dump($mock->plus(1,2));

// vyhodí exception Uncaught Mockery\Exception\NoMatchingExpectationException: No matching handler found for Mockery_0::plus(4, 5)
var_dump($mock->plus(4,5));
