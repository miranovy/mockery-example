# Mockery

[Mockery](mockery.io) jsou užitečná věc pro tvorbu unit testů v php, která umožňuje nahrazovat (simulovat) chování funkcí u tříd.

## Instalace

```bash
composer require --dev mockery/mockery
```

## Integrace

### Integrace s phpunit

Po skončení každého testu je potřeba volat `Mockery::close()`:

```php
<?php declare(strict_types=1);

namespace Tests\MyTest;

use Mockery;
use PHPUnit\Framework\TestCase;

class BaseTestCase extends TestCase
{
    public function tearDown(): void
    {
        Mockery::close();

        parent::tearDown();
    }
}
```

### Integrace s CodeCeption

Volání `Mockery::close()` po každém testu provede *mockery modul*. Ten se nainstaluje příkazem:

```bash
composer require --dev codeception/mockery-module
```

Po instalaci je třeba modul povolit v `unit.suite.yml`:
   ```yaml
   actor: UnitTester
   modules:
      enabled:
        ...
        - Mockery
   ```

## Vytvoření mocku

```php
use Mockery;

// vytvoří mock objekt
$mock = Mockery::mock(); 

// vytvoří mock objekt typu MyClass
$mock = Mockery::mock(MyClass::class); 

// vytvoří mock objekt typu MyInterface
$mock = Mockery::mock(MyInterface::class);
```

## Nastavení očekávaného chování

Zavolání metody bez nastavení očekávaného chování vyhodí chybu.

```php
// vytvoří mock objekt
$mock = Mockery::mock();

// vyhodí chybu Uncaught Mockery\Exception\BadMethodCallException: Method Mockery_0::getString() does not exist on this mock object
var_dump($mock->getNumber());
```

```php
// vytvoří mock objekt
$mock = Mockery::mock();

// nastaví očekávané chování metody getNumber - vrátí hodnotu 5
$mock->shouldReceive('getNumber')->andReturn(5);

// vypíše int(5)
var_dump($mock->getNumber());
```

Pokud chceme nastavovat očekávané chování u protected a private metod je třeba to povolit:

```php
class MyClass
{
    protected function getNumber(): int
    {
	return 5;
    }
}

// vytvoří mock objekt
$mock = Mockery::mock(MyClass::class)->shouldAllowMockingProtectedMethods();

// nastaví očekávané chování metody getNumber (vrátí hodnotu 3)
$mock->shouldReceive('getNumber')->andReturn(3);

// vypíše int(3)
var_dump($mock->getNumber());
```

#### Volání reálné metody

```php
class MyClass
{
    public function getNumber(): int
    {
	return 5;
    }
}

// vytvoří mock objekt
$mock = Mockery::mock(MyClass::class);

// nastaví volání reálné metody getNumber (vrátí hodnotu 5)
$mock->shouldReceive('getNumber')->passthru();

// vypíše int(5)
var_dump($mock->getNumber());
```

### Validace argumentů

```php
// vytvoří mock objekt
$mock = Mockery::mock();

// nastaví očekávané chování metody plus - vrátí hodnotu 3
$mock->shouldReceive('plus')->with(1,2)->andReturn(3);

// vypíše int(3)
var_dump($mock->plus(1,2));

// vyhodí exception Uncaught Mockery\Exception\NoMatchingExpectationException: No matching handler found for Mockery_0::plus(4, 5)
var_dump($mock->plus(4,5));
```

### Požadovaný počet volání

```php
$n=2;

// vytvoří mock objekt
$mock = Mockery::mock();

// nastaví očekávané chování metody plus, která je volání přávě n krát (vrátí hodnotu 3)
$mock->shouldReceive('getNumber')
    ->times($n)
    ->andReturn(3);

// první volání vypíše int(3)
var_dump($mock->getNumber());

// druhé volání vypíše int(3)
var_dump($mock->getNumber());

// třetí volání vypíše int(3)
var_dump($mock->getNumber());

// vyhodí exception Uncaught Mockery\Exception\InvalidCountException: Method getNumber(<Any Arguments>) from Mockery_0 should be called
Mockery::close();
```

Další možnosti jsou:
* `zeroOrMoreTimes()`
* `once()`
* `twice()`
* `never()`
